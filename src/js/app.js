import LoadSVG from 'plugins/localstorage-svg-cache';
import GoogleFonts from 'plugins/google-fonts-async';
import MobileNavToggle from 'plugins/mobile-nav-toggle.js';
import SlideGallery from 'plugins/slide-gallery';

const defaults = {
  breakpoints : [540, 820], // media breakpoints
}

// Assets
let loadSVG = new LoadSVG('svg/svg-sprite.svg');
let googleFonts = new GoogleFonts({
  google: { 
    families: [
      'Open+Sans:400,400italic,600,700,800:latin,cyrillic'
    ] 
  }
});


let mobileNavToggle = new MobileNavToggle({
  hideAfter : defaults.breakpoints[1]
});

let slideGalleryNode = document.querySelector('[data-component="slide-gallery"]');
if(slideGalleryNode) {
  let slideGallery = new SlideGallery({});
}



let calendarBox = document.querySelector('[data-component="calendar"]');
if(calendarBox) {
  require.ensure(['vendor/pikaday'], function(require) {
    const pikaday = require('vendor/pikaday')
    let calendar = new pikaday({
      field: document.querySelector('[data-calendar="datepicker"]'),
      firstDay: 1,
      minDate : new Date(2000, 0, 1),
      maxDate : new Date(2020, 12, 31),
      yearRange : [2000, 2020],
      bound : false,
      showDaysInNextAndPreviousMonths : true,
      i18n : {
        previousMonth : 'Предыдущий месяц',
        nextMonth     : 'Следующий месяц',
        months        : [
          'Январь',
          'Февраль',
          'Март',
          'Апрель',
          'Май',
          'Июнь',
          'Июль',
          'Август',
          'Сентябрь',
          'Октябрь',
          'Ноябрь',
          'Декабрь'
        ],
        weekdays      : [
          'Воскресенье',
          'Понедельник',
          'Вторник',
          'Среда',
          'Четверг',
          'Пятница',
          'Суббота'
        ],
        weekdaysShort : [
          'Вс',
          'Пн',
          'Вт',
          'Ср',
          'Чт',
          'Пт',
          'Сб'
        ]
      },
      container: calendarBox,
      onOpen() {
      },
      onSelect() {
      },
      onClose() {
      },
      onDraw() {
      },
    });
  });
}


let overlay = document.querySelector('[data-component="overlay"]');
if(overlay) {
  (function() {
    let 
      parents = document.querySelector('body, html'),
      modals = overlay.querySelectorAll('[data-overlay="modal"]'),
      triggers = document.querySelectorAll('[data-onclick="modal"]');

    [].slice.call(triggers).forEach((el, i) => {
      el.addEventListener('click', (e) => {
        e.preventDefault();
        let currentModal;

        [].slice.call(modals).forEach((el, i) => {
          if(el.id == e.target.dataset.modalId) {
            overlay.classList.add('is-visible');
            el.classList.add('is-visible');
            parents.classList.add('has-overlay');
            el.querySelector('[data-overlay="close"]').addEventListener('click', (e) => {
              el.classList.remove('is-visible');
              overlay.classList.remove('is-visible');
              parents.classList.remove('has-overlay');
            })
          }
        });

      });
    });

  })();
}