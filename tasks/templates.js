'use strict';

const 
  gulp     = require('gulp'),
  combiner = require('stream-combiner2').obj,
  $        = require('gulp-load-plugins')();

module.exports = function(options) {
  return function() {
    return combiner(
      gulp.src(options.src),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':src'})),
      $.jade({
        locals : options.data,
        pretty : true,
      }),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':compiled'})),
      gulp.dest(options.dest),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':dest'}))
    ).on('error', $.notify.onError(function(err) {
      let errorArray = err.message.replace(/\\+r/, '').split('\n');
      let errorOccuredAt = errorArray[0].replace(/\\/g, '/').split('/');
      let errorCode = errorArray[3].replace(/(>|\d+\||\r)/g, ' ').replace(/\s+/, ' ');
      
      console.error(err.message);
        return {
          title : `Jade - ${errorOccuredAt[errorOccuredAt.length - 1]}`,
          message: `${errorCode}`
        };

    }));  
  };
};